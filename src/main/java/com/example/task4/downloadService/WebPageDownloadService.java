package com.example.task4.downloadService;

import org.springframework.stereotype.Component;

@Component
public interface WebPageDownloadService {
    String download();
}

package com.example.task4.downloadService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Profile("opennet")
public class OpennetDownloadServiceImpl extends AbstractDownloadService {

    @Value("${link}")
    private String link;

    @Override
    public String download() {
        try {
            return getHTMLFromResponse(link);
        } catch (IOException | InterruptedException e) {

            e.printStackTrace();
        }
        return null;
    }
}

package com.example.task4.downloadService;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public abstract class AbstractDownloadService implements WebPageDownloadService {

    protected HttpRequest sendRequest(String link) {
        return HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(link))
                .setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11")
                .build();
    }

    protected String getHTMLFromResponse(String link) throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        return client.send(sendRequest(link), HttpResponse.BodyHandlers.ofString()).body();
    }

}

package com.example.task4.counterService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Component
@Profile("opennet")
public class OpennetNewsCounterServiceImpl extends AbstractCounterService {

    @Value("${today.pattern}")
    private String regexp;

    @Value("${date.format}")
    private String dateFormat;

    private String changeDateFormat(LocalDate startDateFormat) {
        return new SimpleDateFormat(dateFormat)
                .format(Date.from(startDateFormat.atStartOfDay(ZoneId.systemDefault()).toInstant()));
    }

    @Override
    public int count(String html) {
        return getNumberOfArticlesByDate(
                regexp.replaceAll("changeFormatDate", changeDateFormat(LocalDate.now()).replaceAll("\\.", "\\.")) //replace for change date format from yyyy-mm-dd to dd.mm.yyyy
                , html
        );
    }
}

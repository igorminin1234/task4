package com.example.task4.counterService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractCounterService implements NewsCounterService {

    protected int getNumberOfArticlesByDate(String regexp, String html) {
        Pattern pat = Pattern.compile(regexp);
        int numberOfArticles = 0;
        if (html != null) {
            Matcher match = pat.matcher(html);
            while (match.find()) {
                numberOfArticles++;
            }
        }
        return numberOfArticles;
    }

}

package com.example.task4.counterService;

import org.springframework.stereotype.Component;

@Component
public interface NewsCounterService {
    int count(String html);
}

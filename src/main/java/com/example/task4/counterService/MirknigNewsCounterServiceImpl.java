package com.example.task4.counterService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("mirknig")
public class MirknigNewsCounterServiceImpl extends AbstractCounterService {

    @Value("${today.pattern}")
    private String regexp;

    @Override
    public int count(String html) {
        return getNumberOfArticlesByDate(regexp, html);
    }
}

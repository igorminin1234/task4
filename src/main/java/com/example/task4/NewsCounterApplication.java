package com.example.task4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewsCounterApplication {

    private static NewsCounterUcs newsCounterUcs;

    public NewsCounterApplication(NewsCounterUcs newsCounterUcs) {
        NewsCounterApplication.newsCounterUcs = newsCounterUcs;
    }

    public static void main(String[] args) {
        SpringApplication.run(NewsCounterApplication.class, args);
        newsCounterUcs.run();
    }

}

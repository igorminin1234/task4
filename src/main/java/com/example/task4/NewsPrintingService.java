package com.example.task4;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class NewsPrintingService {

    @Value("${link}")
    private String link;

    public void printNewsCount(int numberOfArticles) {
        System.out.println("The amount of today's news on " + link + " is " + numberOfArticles);
    }
}

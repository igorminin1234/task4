package com.example.task4;

import com.example.task4.counterService.NewsCounterService;
import com.example.task4.downloadService.WebPageDownloadService;
import org.springframework.stereotype.Component;

@Component
public class NewsCounterUcs {
    final WebPageDownloadService webPageDownloadService;
    final NewsCounterService newsCounterService;
    final NewsPrintingService newsPrintingService;

    public NewsCounterUcs(WebPageDownloadService webPageDownloadService, NewsCounterService newsCounterService, NewsPrintingService newsPrintingService) {
        this.webPageDownloadService = webPageDownloadService;
        this.newsCounterService = newsCounterService;
        this.newsPrintingService = newsPrintingService;
    }

    public void run() {
        String html = webPageDownloadService.download();
        int numberOfArticles = newsCounterService.count(html);
        newsPrintingService.printNewsCount(numberOfArticles);
    }
}
